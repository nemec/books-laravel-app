<?php

return [

    'api' => [
        'url' => env('QSS_URL'),
        'email' => env('QSS_EMAIL'),
        'password' => env('QSS_PASSWORD')
    ]

];
