<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['isQssAuthenticated']], function () {
    Route::get('/', function () {
        return view('home');
    })->name('home');
    Route::get('/authors', '\App\Http\Controllers\AuthorController@index')->name('authors');
    Route::get('/author/delete/{id}', '\App\Http\Controllers\AuthorController@delete');
    Route::get('/author/{id}', '\App\Http\Controllers\AuthorController@view');
    Route::get('/book', '\App\Http\Controllers\BookController@index');
    Route::post('/book', '\App\Http\Controllers\BookController@create');
    Route::get('/book/delete/{id}', '\App\Http\Controllers\BookController@delete');
});

Route::get('/login', function () {
    return view('login');
})->name('login');

Route::post('/login', '\App\Http\Controllers\LoginController@login')->name('login');
Route::get('/logout', '\App\Http\Controllers\LoginController@logout')->name('logout');






