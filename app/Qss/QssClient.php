<?php

namespace App\Qss;
use GuzzleHttp\Client;

class QssClient
{
	private Client $_client;

	public function __construct($command = false) {
        $opts = ['base_uri' => config('qss.api.url')];

        if ($command) {
            $email = config('qss.api.email');
            $password = config('qss.api.password');
            $client = new self();
            $response = $client->post("token", [ "email" => $email, "password" => $password]);
            $opts['headers'] = ['Authorization' => 'Bearer '.$response['body']->token_key ];
            $this->_client = new Client($opts);
            return;
        }

        if (session('qss_token')) {
            $opts['headers'] = ['Authorization' => 'Bearer '.session('qss_token') ];
        }
		$this->_client = new Client($opts);
	}

	public function post(string $endpoint, array $params) {
		try {
			$request = $this->_client->post($endpoint, ["json" => $params]);
		}
		catch(\Exception $e) {
			$response = json_decode($e->getResponse()->getBody()->getContents(), true);
			return $response;
		}
        $response['body'] = json_decode($request->getBody());
        $response['code'] = $request->getStatusCode();
		return $response;
	}

	public function get(string $endpoint, array $params = []) {
		try {
			$request = $this->_client->get($endpoint, $params);
		}
		catch(\Exception $e) {
            $response = json_decode($e->getResponse()->getBody()->getContents(), true);
			return $response;
		}
		$response['body'] = json_decode($request->getBody());
        $response['code'] = $request->getStatusCode();
		return $response;
	}

    public function delete(string $endpoint) {
		try {
			$request = $this->_client->delete($endpoint);
		}
		catch(\Exception $e) {
			$response = json_decode($e->getResponse()->getBody()->getContents(), true);
			return $response;
		}
		$response['body'] = json_decode($request->getBody());
        $response['code'] = $request->getStatusCode();
		return $response;
	}

}
