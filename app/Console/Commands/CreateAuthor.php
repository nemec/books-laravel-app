<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Qss\QssClient;

class CreateAuthor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @author array
     * Example
     * ["first_name": "Ivan","last_name": "Nemec","birthday": "04-04-1986","biography": "my own","gender": "male","place_of_birth": "SB"]
     *
     */
    protected $signature = 'author:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create author';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $author = [];
        $author['first_name'] = $this->ask('First name?');
        $author['last_name'] = $this->ask('Last name?');
        $author['birthday'] = $this->ask('Birthday?');
        $author['biography'] = $this->ask('Biography?');
        $author['gender'] = $this->choice('Gender?', ['male', 'female']);
        $author['place_of_birth'] = $this->ask('Place of birth?');

        $this->createAuthor($author);
    }

    private function createAuthor($author) {
        $client = new QssClient(true);
        $author = $client->post('authors', $author);
        dd($author);
    }
}
