<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Qss\QssClient;

class BookController extends Controller
{

    public function index($errors = []) {
        $authors = AuthorController::getAuthors();
        return view('book')->with(['authors' => $authors])->withErrors($errors);
    }

    public function create(Request $request) {
        $data = [
            'author' => [ 'id' => (int)$request->author ],
            'title' => $request->title,
            'release_date' => $request->release_date,
            'description' => $request->description,
            'isbn' => $request->isbn,
            'format' => $request->format,
            'number_of_pages' => (int)$request->number_of_pages
        ];

        $client = new QssClient();
        $response = $client->post('books', $data);

        if (isset($response['error'])) {
            return $this->index(['error' => $response['error']]);
        }
        return redirect('/author/'.$request->author);
    }

    public function delete($id) {
        $client = new QssClient();
        $response = $client->delete('books/'.$id);
        return redirect()->back()->withErrors(['message' => 'Book #'.$id.' deleted']);
    }
}
