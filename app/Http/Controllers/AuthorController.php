<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Qss\QssClient;

class AuthorController extends Controller
{

	public function index() {
		$authors = self::getAuthors();
		return view('authors')->with(['authors' => $authors]);
	}

	public function delete($id) {
		$client = new QssClient();
		$author = self::getAuthor($id);
		if (count($author->books) > 0) {
            return redirect('/author/'.$author->id)->withErrors(['message' => 'This author has books so it can not be deleted!']);
		}
        $response = $client->delete('authors/'.$id);
		return redirect('/authors');
	}

	public function view($id) {
		$author = self::getAuthor($id);
		return view('author')->with(['author' => $author]);
	}

	public static function getAuthor($id) {
		$client = new QssClient();
		$response = $client->get('authors/'.$id);
		return $response['body'];
	}

	public static function getAuthors() {
		$client = new QssClient();
		$response = $client->get('authors?limit=100&direction=desc');
		return $response['body']->items;
	}
}
