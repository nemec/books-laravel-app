<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Qss\QssClient;

class LoginController extends Controller
{
    public function login(Request $request) {
        $client = new QssClient();
        $response = $client->post("token", [ "email" => $request->email, "password" => $request->password]);

        if (isset($response['error'])) {
            return view('login')->withErrors(['error' => $response['errors']]);
        }

        session([
            'qss_token' => $response['body']->token_key,
            'refresh_token_key' => $response['body']->refresh_token_key,
            'user' => ['email' => $response['body']->user->email, 'first_name' => $response['body']->user->first_name, 'last_name' => $response['body']->user->last_name]
        ]);

        return redirect('/');
    }

    public function logout() {
        session()->flush();
        return redirect('/login');
    }
}
