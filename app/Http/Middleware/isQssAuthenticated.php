<?php

namespace App\Http\Middleware;

use Closure;
use App\Qss\QssClient;

class isQssAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!session('qss_token')) {
            return redirect('/logout');
        }

        $client = new QssClient();
        $response = $client->get('me');
        if ($response['code'] == 401) {
            return redirect('/logout');
        }

        return $next($request);
    }
}
