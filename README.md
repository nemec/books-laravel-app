1. git clone git@gitlab.com:nemec/books-laravel-app.git
2. cd books-laravel-app
3. composer install
4. mv .env.example .env
5. php artisan key:generate
6. edit .env and add env variables:

- QSS_URL=https://symfony-skeleton.q-tests.com/api/v2/
- QSS_EMAIL=
- QSS_PASSWORD=


CLI command to add authors:

php artisan author:create

