@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create new book</div>
				<div class="panel-body">
                    @if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/book') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Author</label>
							<div class="col-md-6">
								<select name="author" class="form-control">
                                    @foreach ($authors as $author)
                                        <option value="{{ $author->id }}">{{ $author->first_name }} {{ $author->last_name }}</option>
                                    @endforeach
                                </select>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="title">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">Release date</label>
							<div class="col-md-6">
								<input type="date" class="form-control" name="release_date">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">Description</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="description">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">ISBN</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="isbn">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">Format</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="format">
							</div>
						</div>

                        <div class="form-group">
							<label class="col-md-4 control-label">Number of pages</label>
							<div class="col-md-6">
								<input type="number" class="form-control" name="number_of_pages">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
