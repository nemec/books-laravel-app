@extends('app')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading row" style="margin-left: 0px; margin-right: 0px">
                    <div class="col-md-6">Author # {{ $author->id }}</div>
                    <div class="col-md-6 text-right">
                        <?php
                            $deleteDisabled = 'disabled';
                            if (count($author->books) == 0) {
                                $deleteDisabled = '';
                            }
                        ?>
                        <a href="/author/delete/{{ $author->id }}" class="btn btn-danger text-right" {{ $deleteDisabled }}>Delete</a>
                    </div>
                </div>
				<div class="panel-body">
                    @if (count($errors) > 0)
						<div class="alert alert-danger">
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <b>First Name:</b> {{ $author->first_name }}<br>
                    <b>Last Name:</b> {{ $author->last_name }}<br>
                    <b>Birthday:</b> {{ \Carbon\Carbon::parse($author->birthday)->format('d-m-Y')  }}<br>
                    <b>Gender:</b> {{ $author->gender }}<br>
                    <b>Place of birth:</b> {{ $author->place_of_birth }}<br>
                    <h3>Books</h3>
                    <table id="books" class="display" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Release Date</th>
                                <th>ISBN</th>
                                <th>Format</th>
                                <th>Number of pages</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($author->books as $book)
                                <tr>
                                    <td>{{ $book->id }}</td>
                                    <td>{{ $book->title }}</td>
                                    <td>{{ \Carbon\Carbon::parse($book->release_date)->format('d-m-Y') }}</td>
                                    <td>{{ $book->isbn }}</td>
                                    <td>{{ $book->format }}</td>
                                    <td>{{ $book->number_of_pages }}</td>
                                    <td><a href="/book/delete/{{ $book->id }}">Delete</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
